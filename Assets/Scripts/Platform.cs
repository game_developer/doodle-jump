﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class Platform : MonoBehaviour 
{
    public List<Color> colors;
    public float pushStrength = 10f;

    private SpriteRenderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();

        int randomIndex = Random.Range(0, colors.Count);
        _renderer.color = colors[randomIndex];
    }
}
