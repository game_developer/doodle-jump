﻿using System.Collections;
using System.Linq;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour
{
    public GameObject[] platformPrefabs;
    public float[] chances;

    public float platformHeight, platformWidth;
    public int amount = 45;

    private GameObject player;
    private float spawnHeight;
    private float previousYPosition;
    private float minX, maxX;
    private float screenHeight;
    private const float offset = 5f;

    private float minY = 0.1f, maxY = 0.3f;
    private float difficultyValue = 0.02f;

    private void Start()
    {
        player = GameObject.Find("Player");

        minX = Camera.main.ScreenToWorldPoint(Vector2.zero).x + platformWidth;
        maxX = minX * -1;

        screenHeight = Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y;
        previousYPosition = GameObject.Find("Platform_Default").transform.position.y;

        StartCoroutine(Spawn());
    }

    private void Update()
    {
        if (player.transform.position.y >= spawnHeight)
        {
            StartCoroutine(Spawn());
        }

        RemovePlatform();
    }

    private void RemovePlatform()
    {
        GameObject lowestPlatform = transform.GetChild(0).gameObject;
        float lowestPointY = Camera.main.ScreenToWorldPoint(Vector2.zero).y;

        if (lowestPointY > lowestPlatform.transform.position.y + platformHeight)
        {
            Destroy(lowestPlatform);
        }
    }

    private int PickRandomPlatform()
    {
        float rnd = Random.Range(0f, chances.Sum());

        int index = -1;
        float treshold = 0;

        do
        {
            treshold += chances[index + 1];
            index++;
        } while (treshold <= rnd);

        return index;
    }

    IEnumerator Spawn()
    {
        //SO THAT IT WON'T CALL THIS METHOD IN THE UPDATE SECTION AGAIN
        spawnHeight += 10f;

        for (int i = 0; i < amount; i++)
        {
            float xPos = Random.Range(minX, maxX);
            float yPos = previousYPosition + platformHeight + Random.Range(minY, maxY);

            int index = PickRandomPlatform();

            Instantiate(platformPrefabs[index], new Vector3(xPos, yPos), Quaternion.identity, transform);

            previousYPosition = yPos;

            yield return null;
        }
        spawnHeight = previousYPosition - screenHeight - offset;

        MakeMoreDifficult();
    }

    private void MakeMoreDifficult()
    {
        if (maxY - minY > 0.13f)
        {
            minY += difficultyValue;
        }
        else
        {
            maxY += difficultyValue;
        }

        difficultyValue *= 0.98f;
    }
}
