﻿using UnityEngine;

public class Bug : MonoBehaviour
{
    public float speed = 2f;
    private Rigidbody2D rb;

    private float yPos;

    private void Start()
    {
        yPos = transform.position.y;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        float x = Mathf.Cos(Time.time * speed) * 1.7f;

        var position = transform.position;
        position.x = x;
        transform.position = position;
    }
}
