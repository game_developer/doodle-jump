﻿using UnityEngine;

public class BugSpawner : MonoBehaviour
{
    public GameObject bug;
    public float bugHeight;

    private float spawnPointY;
    private float spawnIterator = 15f;
    private GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (player.transform.position.y >= spawnPointY)
        {
            Spawn();
        }

        DestroyBugs();
    }

    private void Spawn()
    {
        spawnPointY += spawnIterator;

        float bugPosX = bug.transform.position.x;

        Instantiate(bug, new Vector2(bugPosX, spawnPointY), Quaternion.identity, transform);
        
    }

    private void DestroyBugs()
    {
        if (transform.childCount > 0)
        {
            var lastBug = transform.GetChild(0);

            if (lastBug.position.y + bugHeight < Camera.main.ScreenToWorldPoint(Vector2.zero).y)
            {
                Destroy(lastBug.gameObject);
            } 
        }
    }
}
