﻿using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    private TextMeshProUGUI tmPro;
    private GameObject _camera;
    private float score = 0;

    private void Start()
    {
        _camera = GameObject.Find("Main Camera");
        tmPro = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        float score = _camera.transform.position.y;
        score = Mathf.Round(score * 3f);

        tmPro.SetText(score.ToString());
        
    }
}
