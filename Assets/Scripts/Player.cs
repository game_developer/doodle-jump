﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour 
{
    private Rigidbody2D rb;
    public float sideSpeed = 30f;
    public float playerWidth;

    private float endX;

    private void Start()
    {
        endX = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x + playerWidth;
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            SceneManager.LoadScene("Main");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Platform")
        {
            //WHEN FALLS DOWN
            if (rb.velocity.y < 0)
            {
                float pushStrength = collision.GetComponent<Platform>().pushStrength;

                Vector2 rbVelocity = rb.velocity;
                rbVelocity.y = pushStrength;
                rb.velocity = rbVelocity;
            }
        }
    }

    private void Update()
    {
        Move();
        SideWalk();
        CheckDeath();
    }

    private void Move()
    {
        float axis = Input.GetAxis("Horizontal");

        Vector2 velocity = rb.velocity;
        if (axis != 0)
        {
            velocity.x = axis * sideSpeed * Time.deltaTime;
            rb.velocity = velocity;
        }

        else
        {
            velocity.x = 0;
            rb.velocity = velocity;
        }
    }

    private void CheckDeath()
    {
        if (rb.position.y < Camera.main.ScreenToWorldPoint(Vector2.zero).y)
        {
            SceneManager.LoadScene("Main");
        }
    }

    private void SideWalk()
    {
        Vector2 pos = transform.position;

        if (rb.position.x >= endX)
        {
            pos.x = -endX + 0.01f;

        }

        else if (rb.position.x <= -endX)
        {
            pos.x = endX - 0.01f;
        }

        transform.position = pos;
    }
}
